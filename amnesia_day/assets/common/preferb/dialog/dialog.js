// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html
var com = require('globalVar');
// 台词格式：
// 0: xxxxx
// NPC1: xxxxx
// NPC2: xxxxx
// 脚本会自动匹配角色和头像，并自动处理名称，0会自动替换成玩家设定的名字，其他NPC会将名字抠出来原封不动使用


cc.Class({
    extends: cc.Component,

    properties: {
        tc_content:{
		default:null,
		type:cc.Label
	},
	tc:{
		default:null,
		type:cc.Label
	},
	interval:1,
	tc_content:{
		default:null,
		type:cc.Label
	},
	player_name_label:{
		default:null,
		type:cc.Label
	},
	head:{
		default:null,
		type:cc.Sprite
	},
	name_tc:{
		default:null,
		type:cc.Node
	},
	headers:{
		default:[],
		type:[cc.SpriteFrame]
	},
	headers_role_name:{
		default:[],
		type:cc.String
	},
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
		this.tc_contents = this.tc_content.string.split('\n');
		this.tc_p = 0;
		this.head_X = this.head.node.x;
		this.name_tcx=this.name_tc.x;
		this.player_name_X = this.player_name_label.node.x;
		this.nowGoal = -1;
		this.nextTc();
    },
	resetTC:function(content){
		if (!this.node.active){
			this.tc_contents = content.split("\n");
			if (this.tc_contents.length==1){
				this.tc_contents = content.split("n");
			}
			this.tc_p = 0;
			this.nowGoal = -1;
			this.node.active = true;
			this.nextTc();
		}
	},
	moveLeft:function(){
		this.name_tc.removeComponent(cc.Widget);
		this.head.node.removeComponent(cc.Widget);
		this.player_name_label.node.removeComponent(cc.Widget);
		this.head.node.x = -Math.abs(this.head_X);
		this.name_tc.x = Math.abs(this.name_tcx);//0.2
		this.player_name_label.node.x = -this.name_tc.width/2;
		
	},
	moveRight:function(){
		this.name_tc.removeComponent(cc.Widget);
		this.head.node.removeComponent(cc.Widget);
		this.player_name_label.node.removeComponent(cc.Widget);
		this.head.node.x = Math.abs(this.head_X);
		this.name_tc.x = -Math.abs(this.name_tcx);//0.2
		this.player_name_label.node.x = Math.abs(this.player_name_X)-this.player_name_label.node.width;
	},
	NoRole:function(){
		this.head.node.active = false;
		this.name_tc.x = 0;
		this.name_tc.width = this.node.width;
		
	},
	dealContent:function(content){
		var date = new Date();
		var res = content.replace(/{}/g,com.player_name);
		res = res.replace(/day/g,date.Format("yyyy.MM.dd"));
		return res.substring(res.search("：")+1,res.length);
	},
	finished:function(){
		if (this.node.finishCallBack){
			this.node.finishCallBack();
		}
	},
	nextTc:function(){
		if (this.tc.string.length<this.nowGoal){
			
			this.tc.string = this.dealContent(this.tc_contents[this.tc_p-1]);
			this.nowGoal = -1;
			this.unscheduleAllCallbacks();
			return;
		}
		var content = this.tc_contents[this.tc_p++];
		if (content===undefined)
			this.finished();
		// 获取当前角色，并决定头像框所在的位置
		var splitePoint = content.search("：");
		
		var role = content.substring(0,content.search("："));
		for (var i=0;i<this.headers_role_name.length;i++){
			if (role===this.headers_role_name[i]){
				this.head.spriteFrame = this.headers[i]
			}
		}
		if (role ==='0'||role ==='1'){
			// 为玩家设定的名字
			role = com.player_name;
			//将头像设定在左边
			this.moveLeft();
			this.player_name_label.string = role
		}else if (role.length>0){
			this.moveRight();
			this.player_name_label.string = role
		}
	
		;
		content = this.dealContent(content);
		
		this.nowGoal = content.length;
		this.tc.string = "";
		var p;
		p=0;
		this.unscheduleAllCallbacks();
		this.schedule(function() {
			// 这里的 this 指向 component
			this.tc.string+=content[p++];
		}, this.interval, content.length-1);
		
	}
    // update (dt) {},
});
