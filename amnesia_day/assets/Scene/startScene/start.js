﻿// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

var com = require('globalVar');
 Date.prototype.Format= function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
		timelabel:{
			default:null,
			type:cc.Label
		},
		memory:{
			default:null,
			type:cc.Label
		},
		tc_content:{
			default:null,
			type:cc.Label
		},
		tc:{
			default:null,
			type:cc.Label
		},
		interval:1,
		tcc:{
			default:null,
			type:cc.Label
		},
		init:{
			default:null,
			type:cc.Label
		},
		inputbox:{
			default:null,
			type:cc.EditBox
		},
		phone:{
			default:null,
			type:cc.Sprite
		},
		bed:{
			default:null,
			type:cc.Sprite
		},
		player_name:"&#x",
		confirmButton:{
			default:null,
			type:cc.Button
		},
		musicNode:{
			default:null,
			type:cc.Node
		},
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
		var date = new Date();
        
		this.timelabel.string = date.Format("yyyy.MM.dd");
		this.timelabel.node.active = false;
		this.memory.node.active = false;
		//this.node.on(cc.Node.EventType.TOUCH_END,this.clickStart,this);
		this.tc_p = 0;
		this.tc_content = this.tc_content.string.split('\n');
		this.nowGoal = -1;
		this.inputbox.enabled = false;
		this.confirmButton.node.active=false;
		this.phone.node.active = false;
		this.bed.node.active = false;
		this.init.node.active=false;
		
		cc.game.addPersistRootNode(this.musicNode);
	},

    start () {

    },
	clickStart:function(ev){
		this.node.on(cc.Node.EventType.TOUCH_END,this.nextTc,this);
		this.node.off(cc.Node.EventType.TOUCH_END,this.clickStart,this);
		this.tcc.enabled=false;
		this.init.node.active=true;
		this.nextTc();
	},
    checkEvent:function(){
		if (this.tc.string.match("我的名字应该是")){
			this.inputbox.enabled = true;
			this.confirmButton.node.active=true;
			return true;
		}
		if (this.tc.string.match("我现在应该做的是")){
			this.phone.node.active = true;
			this.bed.node.active = true;
			this.tc.enabled = false;
			this.init.node.active=false;
			return true;
		}
		if (this.tc.string.match(this.timelabel.string)){
			this.timelabel.node.active = true;
		}
		if (this.tc.string.match("失忆")){
			this.memory.node.active = true;
		}
		return false;
	},
	tc_deal:function(content){
		var res = content.replace(/{}/g,com.player_name);
		var res = res.replace("time",this.timelabel.string);
		return res;
	}
	,
	nextTc:function(ev){
		if (this.tc.string.length<this.nowGoal){
			this.tc.string = this.tc_content[this.tc_p-1];
			this.nowGoal = -1;
			this.unscheduleAllCallbacks();
			return;
		}
		if (this.checkEvent())
			return;
		var content = this.tc_content[this.tc_p++];
		this.tc_content[this.tc_p-1] = this.tc_deal(content);
		content = this.tc_content[this.tc_p-1];
		this.nowGoal = content.length;
		this.tc.string = "";
		var p;
		p=0;
		this.unscheduleAllCallbacks();
		this.schedule(function() {
			// 这里的 this 指向 component
			this.tc.string+=content[p++];
			
		}, this.interval, content.length-1);
	},
	confirmName:function(){
		
		com.player_name = this.inputbox.string;
		
		this.confirmButton.node.active = false;
		this.inputbox.node.active = false;
		this.nowGoal = 0;
		this.tc.string = "";
		this.nextTc();
	}
    // update (dt) {},
});
