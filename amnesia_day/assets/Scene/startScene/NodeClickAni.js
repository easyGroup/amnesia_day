﻿// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html
// 节点动画绑定
cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
		another:{
			default:null,
			type:cc.Sprite
		},
		nextScene:"fail",
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
		this.ani = this.getComponent(cc.Animation);
		this.another.ani1 = this.another.getComponent(cc.Animation);
		this.node.on(cc.Node.EventType.MOUSE_MOVE,this.moveOn,this);
		this.node.on(cc.Node.EventType.TOUCH_START,this.touchStart,this);
		this.node.on(cc.Node.EventType.TOUCH_END,this.touchEnd,this);
	},

	moveOn:function(){
		this.node.opacity = 255;
		this.another.node.opacity = 200;
	},
	
	touchStart:function(){
		this.another.node.active = false;
	},
	touchEnd:function(){
		if(this.node.name==="phone")
			this.ani.play("phone");
		else
			this.ani.play("bed");
		this.another.ani1.play("go");
	},
	nextScene1:function(){
		
		cc.director.loadScene(this.nextScene);
	},
	
	
    start () {

    },
	
    // update (dt) {},
});
