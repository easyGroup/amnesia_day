// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
		background:{
			default:null,
			type:cc.Sprite
		},
		init:{
		default:null,
		type:cc.Label
	},
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
		this.ani = this.getComponent(cc.Animation);
		this.startShow = true;
	},
	finishshow:function(){
		this.background.node.on(cc.Node.EventType.TOUCH_END,this.destroyme,this);
		this.ani.pause();
		
	},
	finish:function(){
			this.init.node.active=true;
	},
	destroyme:function(){
			var script = this.background.getComponent('start');
			this.ani.resume();
			this.background.node.off(cc.Node.EventType.TOUCH_END,script.destroyme,script);
			this.background.node.on(cc.Node.EventType.TOUCH_END,script.clickStart,script);

	},
    // update (dt) {},
});
