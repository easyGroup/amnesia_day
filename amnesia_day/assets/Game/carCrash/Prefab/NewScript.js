// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
		dialog:{
			default:null,
			type:cc.Node
		},
		timelabel2:{
			default:null,
			type:cc.Label
		},

    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
		//TODO: 实现一开始打开场景时，对话框不显示，在玩家点击屏幕后，对话框显示并进行对话
		this.dialog.active =false;
	
		this.timelabel2.string =  new Date().Format("yyyy.MM.dd");
		this.node.on(cc.Node.EventType.TOUCH_END,function(){
			this.dialog.active = true;
		},this);
		
		var script = this.dialog.getComponent("dialog");
		this.node.on(cc.Node.EventType.TOUCH_END,script.nextTc,script);
		var self = this;
		this.dialog.finishCallBack=function(){
			self.dialog.active =false;
	
		}
    },
	
});