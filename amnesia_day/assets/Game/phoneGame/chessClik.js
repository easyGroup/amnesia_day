// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    

    // use this for initialization
    onLoad: function () {
		cc.director.getPhysicsManager().enabled=true;
        //*
		this.node.on(cc.Node.EventType.TOUCH_START, this.clickThis, this);//*/
		this.lastSelect = 1;
    },

	clickThis:function(ev){
		var ve = []; // 只有两个空白格
		var nowP = [];
		var tp;
		let i,j;
		for(i =0;i<this.node.table.length;i++){
			for(j = 0;j<this.node.table[0].length;j++){
				if (this.node.table[i][j]==this.node.chess_tag){
					nowP.push(cc.p(i,j));
					ve = ve.concat(this.checkAround(cc.p(i,j)));
				}
			}
		}
		if (ve.length){//存在可能的移动方向
			for(i=ve.length-1;i>-1;i--){
				for(j=0;j<nowP.length;j++){
					tp = nowP[j].add(ve[i]);
					if (!(this.node.table[tp.x][tp.y]==-1||this.node.chess_tag==this.node.table[tp.x][tp.y])){
						ve.splice(i,1);//无效移动
						break;
					}
				}
			}
		}
		if (ve.length==0)
			return;
		var nextDirection = ve[0]
		// 大于 1 个可移动方向则轮询(仅做了两个空格的情况)
		if (ve.length>1){
			if (this.lastSelect==0){
				this.lastSelect = 1;
				nextDirection = ve[1];
			}else
				this.lastSelect = 0;
		}
		
		// 移动位置
		// 移动 table
		var goal;
		for(i=0;i<nowP.length;i++){
			goal = nowP[i].add(nextDirection);
			tp = this.node.table[goal.x][goal.y];
			this.node.table[goal.x][goal.y] = this.node.table[nowP[i].x][nowP[i].y];
			this.node.table[nowP[i].x][nowP[i].y] = tp;
			if(tp==this.node.table[goal.x][goal.y])
				nowP.push(nowP[i]);
		}
		
		// 移动节点
		nextDirection.x *= this.node.cell_width;
		nextDirection.y *= this.node.cell_height;
		this.node.x+=nextDirection.x;
		this.node.y+=nextDirection.y;
		if (this.checkWin())
			this.success();
	},
	checkWin:function(){
		//检测是否胜利
		return (this.node.table[1][0]==0 && this.node.table[2][0]==0);
	},
	success:function(){
		//胜利之后的脚本写在这里
		cc.director.loadScene('phonephoto');
	},
	checkAround:function(position){
		var v = [];
		v[0] = cc.v2(0,1);
		v[1] = cc.v2(0,-1);
		v[2] = cc.v2(-1,0);
		v[3] = cc.v2(1,0);
		let tp;
		var res = [];
		for (let i =0;i<v.length;i++){
			tp = position.add(v[i]);
			if (this.node.table[tp.x][tp.y]==-1)
				res.push(v[i]);
		}
		return res;
	}
    // update (dt) {},
});
