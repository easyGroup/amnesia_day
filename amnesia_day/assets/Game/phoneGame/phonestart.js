// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
		timelabel:{
			default:null,
			type:cc.Label
		},
		timelabel2:{
			default:null,
			type:cc.Label
		},
		unlock:{
			default:null,
			type:cc.Sprite
		},
		playtable:{
			default:null,
			type:cc.Sprite
		},
		dialog:{
			default:null,
			type:cc.Node
		},
		dial:{
			default:null,
			type:cc.Sprite
		},
		camera:{
			default:null,
			type:cc.Sprite
		},
		tc_dial:{
			default:null,
			type:cc.Label
		},
		tc_camera:{
			default:null,
			type:cc.Label
		},
		tc_game:{
			default:null,
			type:cc.Label
		},
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
		this.timelabel.string =  new Date().Format("yyyy.MM.dd");
		this.timelabel2.string =  new Date().Format("yyyy.MM.dd");
		this.playtable.node.active = false;
		this.node.finishBD=function(){
				this.getComponent('phonestart').unlock.node.on(cc.Node.EventType.TOUCH_END,function(){
				this.playtable.node.active = true;
			},this.getComponent('phonestart'));
			var th = this.getComponent('phonestart');
			var script = th.dialog.getComponent("dialog");
			th.node.on(cc.Node.EventType.TOUCH_END,script.nextTc,script);
			th.dialog.finishCallBack = function(){
				this.active = false;
			};
			var tc = th.tc_dial.string;
			th.dial.node.on(cc.Node.EventType.TOUCH_END,function(){this.resetTC(tc);},script);
			var tc2 = th.tc_camera.string;
			th.camera.node.on(cc.Node.EventType.TOUCH_END,function(){this.resetTC(tc2);},script);
			var tc3 = th.tc_game.string;
			th.unlock.node.on(cc.Node.EventType.TOUCH_END,function(){this.resetTC(tc3);},script);
			
		}
	},
    
    start () {
		
    },

    // update (dt) {},
});
