// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
		timelabel:{
			default:null,
			type:cc.Label
		},
		dialog:{
			default:null,
			type:cc.Node
		},
		
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
		//获取所有的方块
		//this.blocks();
		var date = new Date();
                
		this.timelabel.string = date.Format("yyyy.MM.dd");
		var script = this.dialog.getComponent("dialog");
		this.node.on(cc.Node.EventType.TOUCH_END,script.nextTc,script);
		var self = this;
		this.dialog.finishCallBack=function(){
			self.dialog.active = false;
		}
		
		var table = this.node.getChildByName('gameTable');
		this.table = table;
		this.blocks = table.children;
		cc.director.getPhysicsManager().enabled = true;
		this.gametime = 0;
		this.nextCheckTime = 1;
	},

    start () {
		
    },
	success:function(){
		cc.director.loadScene("bedroom2");
	},
    update (dt) {
		this.gametime+=dt;
		if (this.gametime>this.nextCheckTime){
			this.nextCheckTime+=1;
			for (var i =0;i<this.blocks.length;i++){
				if (this.blocks[i].name=='bound1'||this.blocks[i].name=='bound2')
					continue;
				if (this.blocks[i].x>this.table.width||this.blocks[i].x<0||this.blocks[i].y>this.table.height){
					return;
				}
			}
			this.success();
		}
	},
});
